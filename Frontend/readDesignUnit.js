
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gDESIGN_COLS = ["id", "name", "description", "address", "phone", "phone2", "fax", "email", "website", "note", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gDESIGN_STT_COL = 0;
const gDESIGN_NAME_COL = 1;
const gDESIGN_DES_COL = 2;
const gDESIGN_ADDR_COL = 3;
const gDESIGN_PHONE_COL = 4;
const gDESIGN_PHONE2_COL = 5;
const gDESIGN_FAX_COL = 6;
const gDESIGN_EMAIL_COL = 7;
const gDESIGN_WEBSITE_COL = 8;
const gDESIGN_NOTE_COL = 9;
const gDESIGN_ACTION_COL = 10;

var table = $("#design-table").DataTable({
    columns: [
        { data: gDESIGN_COLS[gDESIGN_STT_COL] },
        { data: gDESIGN_COLS[gDESIGN_NAME_COL] },
        { data: gDESIGN_COLS[gDESIGN_DES_COL] },
        { data: gDESIGN_COLS[gDESIGN_ADDR_COL] },
        { data: gDESIGN_COLS[gDESIGN_PHONE_COL] },
        { data: gDESIGN_COLS[gDESIGN_PHONE2_COL] },
        { data: gDESIGN_COLS[gDESIGN_FAX_COL] },
        { data: gDESIGN_COLS[gDESIGN_EMAIL_COL] },
        { data: gDESIGN_COLS[gDESIGN_WEBSITE_COL] },
        { data: gDESIGN_COLS[gDESIGN_NOTE_COL] },
        { data: gDESIGN_COLS[gDESIGN_ACTION_COL] },


    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gDESIGN_ACTION_COL,
            defaultContent: `
            <i class="fas fa-edit edit-designunit"></i>
            <i class="fas fa-trash-alt delete-designunit"></i>
    `
        },

    ],
    scrollX: true,
    scrollCollapse: true,

    fixedColumns: true
})
//biến chứa hàng được chọn
var gRowSelected = ''
//biến chứa list design
var glistDesign = [];
//biến chứa id design
var gDesignIdSelected = '';
var gRowDesignSelected = '';
var glistDesignValidateUpdate = [];
//biến chứa user id
var gUserId = '';
//biến chứ header
var gHeader = ''
var gUserName = ''
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    const token = getCookie("token");
    const userId = getCookie("userId");
    const userName = getCookie('username')

    //Gọi API để lấy thông tin người dùng

    //Khai báo xác thực ở headers
    gHeader = {
        Authorization: "Token" + " " + token
    };
    //gán giá trị cho biến userId
    gUserId = Number(userId);
    gUserName = userName;
    console.log('CHECK USER ID', gUserId);
    console.log('CHECK HEADER', gHeader);
    console.log('CHECK USERNAME', gUserName);
    $('#username').html(gUserName)
    //tải trang
    onPageLoading().then((res) => {
        glistDesign = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });
    //create new design unit
    $('#create-design').on('click', function () {
        onBtnAddDesignClick();
    })
    $('#btn-create-designunit').on('click', function () {

        onBtnCreateDesignOnModalClick();
    })
    //edit design unit
    $('#design-table').on('click', '.edit-designunit', function () {

        onBtnEditDesignClick(this);
    })
    $('#btn-update-designunit').on('click', function () {
        onBtnUpdateOnModalClick()
    })

    //xóa design unit
    $('#design-table').on('click', '.delete-designunit', function () {

        onBtnDeleteDesignClick(this);
    })
    $('#btn-confirm-delete-designunit').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })
    //nhấn nút Logout
    $('#btn-logout').on('click', function () {
        onBtnLogOutClick()
    })






})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//hàm gọi api load danh sách design unit từ server khi tải trang
function onPageLoading() {
    console.log('làm tới đây')
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/designunit",
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm thực hiện khi nhấn nút add new design unit
function onBtnAddDesignClick() {
    $("#create-design-modal").modal("show");

}
//hàm thực hiện khi nhấn create design unit trên modal
function onBtnCreateDesignOnModalClick() {
    createNewDesignUnit();
    $("#create-design-modal").modal("hide");
}
//hàm thực hiện khi lúc nút edit
function onBtnEditDesignClick(paramButton) {
    $("#update-investor-modal").modal("show");
    gDesignIdSelected = getDesignIdFromButton(paramButton)
    console.log('check id customer selected', gDesignIdSelected)
    callApiToGetDesignById(gDesignIdSelected).then((res) => {
        console.log('check data customer need to update ress', res);
        showDataUpdateOnForm(res);
    }).catch((err) => {
        console.log(err)
    });
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateDesign(gDesignIdSelected);
    $("#update-investor-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteDesignClick(paramButton) {
    $("#delete-modal").modal("show");
    gDesignIdSelected = getDesignIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedInvestor();
    $("#delete-modal").modal("hide");

}
//hàm thực hiện khi click nút log out 
function onBtnLogOutClick() {
    //
    redirectToLogin();

}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm xóa cookie
function redirectToLogin() {
    setCookie("token", "", 1);
    window.location.href = "../loginForEmployee.html";
}
//Hàm setCookie 
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
//Hàm get Cookie 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//hàm trả ra id design unit khi nhấn nút edit

function getDesignIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowDesignSelected = table.row(vTableRow).data();
    console.log(gRowDesignSelected.id);
    return gRowDesignSelected.id;
}
//hàm gọi api để lấy design unit cần edit theo id
function callApiToGetDesignById(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/designunit/${id}`,
            async: false,
            type: "GET",
            dataType: "json",
            headers: gHeader,
            success: function (res) {
                resolve(res);

            },
            error: function (err) {
                reject(e)
            }
        })
    })


}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(obj) {
    $('#input-name-update').val(obj.name);
    $('#input-description-update').val(obj.description);
    $('#input-address-update').val(obj.address);
    $('#input-phone-update').val(obj.phone);
    $('#input-phone2-update').val(obj.phone2);
    $('#input-fax-update').val(obj.fax);
    $('#input-email-update').val(obj.email);
    $('#input-website-update').val(obj.website);
    $('#input-note-update').val(obj.note);

}

function updateDesign(paramInvestorId) {
    let objData = {
        name: '',
        description: '',
        address: '',
        phone: '',
        phone2: '',
        fax: '',
        email: '',
        website: '',
        note: ''
    }
    let objDataFromForm = getDataOnFormUpdate(objData);
    callApiToUpdateData(paramInvestorId, objDataFromForm).then((res) => {
        console.log(res);
    }).catch((xhr, status, error) => {
        if (xhr.responseJSON.message == "Forbidden") {
            alert("Bạn không có đủ quyền hạn để thực hiện chức năng này")
        } else {
            console.log(xhr, status, error);
            console.log()
        }
    })
    onPageLoading().then((res) => {
        glistDesign = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });;

}




//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {
    obj.name = $('#input-name-update').val();
    obj.description = $('#input-description-update').val();
    obj.address = $('#input-address-update').val();
    obj.phone = $('#input-phone-update').val();
    obj.phone2 = $('#input-phone2-update').val();
    obj.fax = $('#input-fax-update').val();
    obj.email = $('#input-email-update').val();
    obj.website = $('#input-website-update').val();
    obj.note = $('#input-note-update').val();

    return obj
}
//hàm gọi api để update design unit
function callApiToUpdateData(id, paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/designunit/update/${id}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            headers: gHeader,
            data: JSON.stringify(paramOrderObject),
            success: function (res) {
                resolve(res);

            },
            error: function (xhr, status, error) {
                reject(xhr, status, error)
            }
        })
    })

}

//save new design unit
function createNewDesignUnit() {
    let objData = {
        name: '',
        description: '',
        address: '',
        phone: '',
        phone2: '',
        fax: '',
        email: '',
        website: '',
        note: ''
    }
    let objDataFromForm = getDataOnForm(objData);
    var vCheck = validateDataOnForm(objDataFromForm);
    callApiToPostData(objDataFromForm).then(res => {

    }).catch((xhr, status, error) => {
        if (xhr.responseJSON.message == "Forbidden") {
            alert("Bạn không có đủ quyền hạn để thực hiện chức năng này")
        } else {
            console.log(xhr, status, error);
            console.log()
        }
    });
    onPageLoading().then((res) => {
        glistDesign = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });;
}
//hàm validate dữ liệu
function validateDataOnForm(paramObj) {
    if (paramObj.name == "") {
        alert("phải điền tên nhà đầu tư");
        return false
    }
    if (paramObj.phone == "") {
        alert("phải điền số điện thoại");
        return false
    }
    if (paramObj.email == "") {
        alert("phải điền email");
        return false
    }
    return true;
}
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.name = $('#input-name').val();
    obj.description = $('#input-description').val();
    obj.address = $('#input-address').val();
    obj.phone = $('#input-phone').val();
    obj.phone2 = $('#input-phone2').val();
    obj.fax = $('#input-fax').val();
    obj.email = $('#input-email').val();
    obj.website = $('#input-website').val();
    obj.note = $('#input-note').val();
    return obj
}
//hàm gọi api để tạo province mới
function callApiToPostData(paramOrderObject) {
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/designunit/create`,
            type: 'POST',
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramOrderObject),
            headers: gHeader,
            success: function (res) {

                resolve(res)

            },
            error: function (xhr, status, error) {
                reject(xhr, status, error)
            }
        })
    })

}
function deletedInvestor() {
    callApiToDeleteData(gDesignIdSelected);
    onPageLoading().then((res) => {
        glistDesign = res
        console.log('check data get all  from server', res);
        table.clear();
        table.rows.add(res);
        table.draw();
    }).catch((err) => {
        console.log(err)
    });
}
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/designunit/delete/${id}`,
        type: 'DELETE',
        async: false,
        headers: gHeader,
        success: function (res) {
            alert('Delete success')

        },
        error: function (xhr, status, error) {
            if (xhr.responseJSON.message == "Forbidden") {
                alert("Bạn không có đủ quyền hạn để thực hiện chức năng này")
            } else {
                console.log(xhr, status, error);
                console.log()
            }
        }
    })
}




