
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gORDER_COLS = ["id", "comments", "orderDate", "requiredDate", "shippedDate", "status", "customername", "action", "detail"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gORDER_STT_COL = 0;
const gORDER_COMMENT_COL = 1;
const gORDER_ORDERDATE_COL = 2;
const gORDER_REQUIREDDATE_COL = 3;
const gORDER_SHIPPEDDATE_COL = 4;
const gORDER_STATUS_COL = 5;
const gORDER_CUSTOMER_NAME_COL = 6;
const gORDER_ACTION_COL = 7;
const gORDER_DETAIl_COL = 8;

var table = $("#order-table").DataTable({
    columns: [
        { data: gORDER_COLS[gORDER_STT_COL] },
        { data: gORDER_COLS[gORDER_COMMENT_COL] },
        { data: gORDER_COLS[gORDER_ORDERDATE_COL] },
        { data: gORDER_COLS[gORDER_REQUIREDDATE_COL] },
        { data: gORDER_COLS[gORDER_SHIPPEDDATE_COL] },
        { data: gORDER_COLS[gORDER_STATUS_COL] },
        { data: gORDER_COLS[gORDER_CUSTOMER_NAME_COL] },
        { data: gORDER_COLS[gORDER_ACTION_COL] },
        { data: gORDER_COLS[gORDER_DETAIl_COL] }
    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gORDER_ACTION_COL,
            defaultContent: `
    				<i class="fas fa-edit edit-order"></i>
    				
    		  		`
        },
        // {
        //     targets: gORDER_CUSTOMER_NAME_COL,
        //     render: function (data) {
        //         data = gCustomerSelected.firstName + " " + gCustomerSelected.lastName
        //         return data;
        //     },
        // },
        { // định nghĩa lại cột action
            targets: gORDER_DETAIl_COL,
            defaultContent: `
    				<Button class='btn btn-primary btn-order-detail'>Chi tiết Order</Button>

    		  		`
        },
    ],
    // responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    scrollX: true,
    scrollCollapse: true,
    fixedColumns: true
})
table.buttons().container().appendTo('#order-table_wrapper .col-md-6:eq(0)');
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gDETAIL_COLS = ["id", "quantityOrder", "priceEach", "productName", "productPhoto"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gDETAIL_STT_COL = 0;
const gDETAIL_QUAN_COL = 1;
const gDETAIL_PRICE_COL = 2;
const gDETAIL_NAME_COL = 3;
const gDETAIL_PHOTO_COL = 4;


var tableDetail = $("#order-detail-table").DataTable({
    columns: [
        { data: gDETAIL_COLS[gDETAIL_STT_COL] },
        { data: gDETAIL_COLS[gDETAIL_QUAN_COL] },
        { data: gDETAIL_COLS[gDETAIL_PRICE_COL] },
        { data: gDETAIL_COLS[gDETAIL_NAME_COL] },
        { data: gDETAIL_COLS[gDETAIL_PHOTO_COL] },

    ],
    columnDefs: [

        {
            targets: gDETAIL_PHOTO_COL,
            render: function (data) {
                data = `<img src="${data}" width="50" height="60"/>`
                return data;
            },
        },

    ],
    // responsive: true,
    // lengthChange: false,
    // autoWidth: false,
    // scrollX: true,
    // scrollCollapse: true,
    // fixedColumns: true
})
$(document).ready(function () {

    onPageLoading()

    //create new photo
    $('#create-order').on('click', function () {
        onBtnAddOrderClick();
    })
    $('#btn-create-order').on('click', function () {

        onBtnCreateOrderOnModalClick();
    })
    //edit photo
    $('#order-table').on('click', '.edit-order', function () {


        onBtnEditOrderClick(this);
    })
    $('#btn-update-order').on('click', function () {

        onBtnUpdateOnModalClick()
    })

    //xóa photo
    $('#order-table').on('click', '.delete-order', function () {

        onBtnDeleteAlbumClick(this);
    })
    $('#btn-confirm-delete-order').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })
    //click nút chi tiết order
    $('#order-table').on('click', '.btn-order-detail', function () {

        onBtnOrderDetailClick(this);
    })
    //click nút chi tiết order
    $('#btn-confirm-detail-order').on('click', function () {

        onBtnConfirmStatusOnModalClick();
        // onBtnOrderDetailClick(this);
    })

})

var gRowSelected = ''
var gCustomerList = '';
var gCustomerIdSelected = '';
var glistOrder = [];
var gRowPhotoSelected = '';
var gOrderIdSelected = '';
var gCustomerSelected = '';
var gCustomerIdOnUrlPath = ''
var gListProduct = [];
var gListOrderdetail = [];
var gListProductOrder = [];
var gProductOrder = {};

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

function onBtnConfirmStatusOnModalClick() {
    callAPItoConfirmOrder(gOrderIdSelected).then(res => {
        console.log(res);
        console.log('check PRODUCT ORDER', gListProductOrder)
        if (gListProductOrder && gListProductOrder.length > 0) {
            for (let bI = 0; bI < gListProductOrder.length > 0; bI++) {
                callAPIToUpdateProductNumber(gListProductOrder[bI].id, gListProductOrder[bI].buyNumber).then(res => {
                    console.log('CHECK RES PRODUCT UPDATE NUMBER', res)
                    $("#modal-table").modal("hide");
                    onPageLoading()
                }).catch(e => {
                    console.log(e)
                })
            }
        }
    }).catch(e => {
        console.log(e)
    })
}

//hàm thực hiện khi nhấn nút add new photo
function onBtnAddOrderClick() {
    $("#create-order-modal").modal("show");


}

//hàm thực hiện khi lúc nút edit
function onBtnEditOrderClick(paramButton) {
    $("#update-order-modal").modal("show");
    gOrderIdSelected = getOrderIdFromButton(paramButton)

    console.log('check id photo selected', gOrderIdSelected)
    callApiToGetOrderById(gOrderIdSelected)
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateOrder(gOrderIdSelected);
    $("#update-order-modal").modal("hide");


}

//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteAlbumClick(paramButton) {
    $("#delete-modal").modal("show");
    gOrderIdSelected = getOrderIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedOrder();
    $("#delete-modal").modal("hide");

}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function callAPItoConfirmOrder(paramId) {
    return new Promise((resolve, reject) => {
        obj = {
            status: "confirm"
        }
        $.ajax({
            async: false,
            url: `http://localhost:8080/order/update_confirm/${paramId}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(obj),
            success: function (res) {
                resolve(res)

            },
            error: function (error) {
                reject(error)
            }
        })

    })
}
function callAPIToUpdateProductNumber(paramId, paramNumber) {
    return new Promise((resolve, reject) => {
        obj = {
            buyNumber: paramNumber
        }
        $.ajax({
            async: false,
            url: `http://localhost:8080/product/update_confirm/${paramId}`,
            type: 'PUT',
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(obj),
            success: function (res) {
                resolve(res)

            },
            error: function (error) {
                reject(error)
            }
        })

    })
}
//hàm thực hiện khi nhấn nút order detail
function onBtnOrderDetailClick(paramButton) {

    gOrderIdSelected = getOrderIdFromButton(paramButton)
    callApiToGetProduct().then(res => {
        gListProduct = res
        console.log('CHECK PRODUCT', gListProduct)
        console.log('CHECK Order id', gOrderIdSelected)
        callAPIToGetOrderDetailByOrderId(gOrderIdSelected).then(res => {
            gListOrderdetail = res;
            console.log('CHECK ORDER DETAIL', gListOrderdetail)
            for (let bI = 0; bI < gListProduct.length; bI++) {
                gListOrderdetail.map(item => {
                    if (item.idOfProduct == gListProduct[bI].id) {
                        item.productName = gListProduct[bI].productName;
                        item.productPhoto = gListProduct[bI].photo;

                        gListProductOrder.push(gListProduct[bI]);
                        return item
                    }
                    // gProductOrder.number = item.quantityOrder;

                })
                gListProductOrder.map(item => {
                    for (let bI = 0; bI < gListProductOrder.length; bI++) {
                        if (gListOrderdetail[bI].idOfProduct == item.id) {
                            item.buyNumber = gListOrderdetail[bI].quantityOrder
                            return item;
                        }
                    }
                })
            }
            tableDetail.clear();
            tableDetail.rows.add(gListOrderdetail);
            tableDetail.draw();

        });
    }).catch(e => {
        console.log(e)
    })
    $("#modal-table").modal("show");



    // window.location.href = `orderDetailOfOneCustomer.html?id=${gOrderIdSelected}`

    // callApiToGetCustomerById(gCustomerIdSelected)
}
//hàm gọi api để lấy tất cả product
function callApiToGetProduct() {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/product/all",
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm gọi api để  order detail bằng order id
function callAPIToGetOrderDetailByOrderId(paramId) {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: `http://localhost:8080/order/${paramId}/order_detail`,
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}

/*** DELETE-------------------------------------------------------------------------- */
function deletedOrder() {
    callApiToDeleteData(gOrderIdSelected);
    onPageLoading();
}
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/order/delete/${id}`,
        type: 'DELETE',
        async: false,
        // contentType: "application/json;charset=UTF-8",
        // data: JSON.stringify(paramOrderObject),
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}
/*** UPDATE-------------------------------------------------------------------------- */


function updateOrder(paramOrderId) {
    let objData = {
        comments: '',
        requiredDate: '',
        shippedDate: '',
        status: ''
    }

    let objDataFromForm = getDataOnFormUpdate(objData);
    console.log("check UPDATE", objDataFromForm)
    callApiToUpdateData(paramOrderId, objDataFromForm);
    onPageLoading();

}

//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {
    obj.comments = $('#input-comment-update').val();

    obj.requiredDate = $('#input-required-date-update').val();
    obj.shippedDate = $('#input-shipped-date-update').val();
    obj.status = $('#input-status-update').val();
    return obj
}
//hàm gọi api để update album
function callApiToUpdateData(id, paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/order/update/${id}`,
        type: 'PUT',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('check data update photo response', res);

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}


//hàm trả ra id photo khi nhấn nút edit
function getOrderIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowPhotoSelected = $("#order-table").DataTable().row(vTableRow).data();
    console.log(gRowPhotoSelected.id);
    return gRowPhotoSelected.id;
}
//page loading
function onPageLoading() {
    $.ajax({
        url: `http://localhost:8080/order/all`,
        async: false,
        type: "GET",
        dataType: "json",
        success: function (res) {
            callApiToGetCustomer()
            console.log('check order all', res);
            console.log('check photo by id', res);
            glistOrder = res
            for (let bI = 0; bI < gCustomerList.length; bI++) {
                res.map(item => {
                    if (item.idOfCustomer == gCustomerList[bI].id)
                        item.customername = gCustomerList[bI].lastName + " " + gCustomerList[bI].firstName
                    return item
                })
            }

            console.log('CHECK DATA CUSTOM', res)
            $("#order-table").DataTable().clear();
            $("#order-table").DataTable().rows.add(res);
            $("#order-table").DataTable().draw();
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}
//hàm gọi api để lấy photo cần edit theo id
function callApiToGetOrderById(id) {

    $.ajax({
        url: `http://localhost:8080/order/details/${id}`,
        async: false,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check data photo need to update ress', res);
            // if (glistAlbum && glistAlbum.length > 0) {
            // 	glistAlbum.map((item, index) => {
            // 		if (item.id == res.id) {
            // 			glistAlbum.splice(index, 1)
            // 		}
            // 	})
            // }
            showDataUpdateOnForm(res);
        },
        error: function (err) {
            console.log(err.response);
        }
    })

}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(paramPhoto) {
    $('#input-comment-update').val(paramPhoto.comments);

    $('#input-required-date-update').val(paramPhoto.requiredDate);
    $('#input-shipped-date-update').val(paramPhoto.shippedDate);
    $('#input-status-update').val(paramPhoto.status);

}

/*** CREATE-------------------------------------------------------------------------- */
//hàm thực hiện khi nhấn create photo trên modal
function onBtnCreateOrderOnModalClick() {
    createNewOrder();
    $("#create-order-modal").modal("hide");
}
//save new photo
function createNewOrder() {
    let objData = {
        comments: '',

        requiredDate: '',
        shippedDate: '',
        status: ''
    }
    let objDataFromForm = getDataOnForm(objData);
    console.log("check photo object", objDataFromForm)
    callApiToPostData(objDataFromForm);
    onPageLoading();


}

//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.comments = $('#input-comment').val();
    obj.requiredDate = $('#input-required-date').val();
    obj.shippedDate = $('#input-shipped-date').val();
    obj.status = $('#input-status').val();
    return obj
}
//hàm gọi api để tạo album mới
function callApiToPostData(paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/order/create/${gCustomerIdSelected}`,
        type: 'POST',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {

            console.log('check data post response', res);

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}

function callApiToGetCustomer() {
    $.ajax({
        async: false,
        url: "http://localhost:8080/customer/all",
        method: "GET",
        success: function (pObjRes) {
            gCustomerList = pObjRes
            loadDataToUserSelect(gCustomerList);
            console.log('check response customer', pObjRes)

        },
        error: function (pXhrObj) {
            console.log(pXhrObj);
        }
    });
}
var gCustomerSelectElement = $("#customer-select");
//hàm load thông tin album vào ô select
function loadDataToUserSelect(pCustomerList) {

    for (i = 0; i < pCustomerList.length; i++) {
        var bCustomerOption = $("<option/>");
        bCustomerOption.prop("value", pCustomerList[i].id);
        bCustomerOption.prop("text", pCustomerList[i].lastName + ' ' + pCustomerList[i].firstName);
        gCustomerSelectElement.append(bCustomerOption);
    };
}
//hàm xử lí chọn album
gCustomerSelectElement.on("change", function () {
    if (gCustomerList && gCustomerList.length > 0) {
        gCustomerList.map(item => {
            if (item.id == $(this).val()) {
                gCustomerIdSelected = item.id;
                gCustomerSelected = item
            }
        })
    }
    console.log('check customer id', gCustomerIdSelected)
    onChangeSelect();

})
function onChangeSelect() {

    $.ajax({
        url: `http://localhost:8080/customer/${gCustomerIdSelected}/orders`,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check photo by id', res);
            glistOrder = res
            for (let bI = 0; bI < gCustomerList.length; bI++) {
                res.map(item => {
                    if (item.idOfCustomer == gCustomerList[bI].id)
                        item.customername = gCustomerList[bI].lastName + " " + gCustomerList[bI].firstName
                    return item
                })
            }
            console.log('CHECK DATA CUSTOM', res)
            $("#order-table").DataTable().clear();
            $("#order-table").DataTable().rows.add(res);
            $("#order-table").DataTable().draw();
            // console.log('LÀM TỚI ĐÂY', $("#order-table").DataTable().rows.data())
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}









