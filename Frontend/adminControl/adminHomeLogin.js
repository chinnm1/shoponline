$(document).ready(function () {
    $("#btn-logout").on("click", function () {
        onBtnLogOutClick()
    })
    function onBtnLogOutClick() {
        //
        redirectToLogin();

    }
    //hàm xóa cookie
    function redirectToLogin() {
        setCookie("token", "", 1);
        window.location.href = "../loginForEmployee.html";
    }
    //Hàm setCookie 
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
})