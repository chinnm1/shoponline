
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gCUSTOMER_COLS = ["id", "address", "fullname", "phoneNumber", "total", "sumOrder", "action", "detail"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCUSTOMER_STT_COL = 0;
const gCUSTOMER_ADDRESS_COL = 1;
const gCUSTOMER_NAME_COL = 2;
const gCUSTOMER_PHONE_COL = 3;
const gCUSTOMER_TOTAL_COL = 4;
const gCUSTOMER_SUMORDER_COL = 5;
const gCUSTOMER_ACTION_COL = 6;
const gCUSTOMER_DETAIL_COL = 7;


var table = $("#table-customer").DataTable({
    columns: [
        { data: gCUSTOMER_COLS[gCUSTOMER_STT_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_ADDRESS_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_NAME_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_PHONE_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_TOTAL_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_SUMORDER_COL] },

        { data: gCUSTOMER_COLS[gCUSTOMER_ACTION_COL] },
        { data: gCUSTOMER_COLS[gCUSTOMER_DETAIL_COL] }
    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gCUSTOMER_ACTION_COL,
            defaultContent: `
					<i class="fas fa-edit edit-customer"></i>
				
			  
			`
        },
        { // định nghĩa lại cột action
            targets: gCUSTOMER_DETAIL_COL,
            defaultContent: `
					<Button class='btn btn-primary btn-order'>Chi tiết Order</Button>
					<Button class='btn btn-warning btn-payment'>Chi tiết Payment</Button>
			  		`
        },

    ],
    // responsive: true,
    lengthChange: false,
    autoWidth: false,
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
    scrollX: true,
    scrollCollapse: true,
    fixedColumns: true
})
table.buttons().container().appendTo('#table-customer_wrapper .col-md-6:eq(0)');
var gRowSelected = ''
var glistCustomer = [];
var gCustomerIdSelected = '';
var gRowCustomerSelected = '';
var glistCustomerValidateUpdate = [];
var glistOrder = [];
var glistPayment = [];
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading().then(res => {
        glistCustomer = res
        customizeData()
    }).catch(e => {
        console.log(e)
    });
    //export to excel
    $('#export-to-excel').on('click', function () {

        onBtnExportToExcelClick()
    })
    //create new album
    $('#create-customer').on('click', function () {

        onBtnAddCustomerClick();
    })
    $('#btn-create-customer').on('click', function () {

        onBtnCreateCustomerOnModalClick();
    })
    //edit album
    $('#table-customer').on('click', '.edit-customer', function () {

        onBtnEditCustomerClick(this);
    })
    $('#btn-update-album').on('click', function () {
        onBtnUpdateOnModalClick()
    })

    //xóa album
    $('#table-customer').on('click', '.delete-customer', function () {

        onBtnDeleteCustomerClick(this);
    })
    $('#btn-confirm-delete-customer').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })

    //click nút chi tiết order
    $('#table-customer').on('click', '.btn-order', function () {

        onBtnOrderDetailClick(this);
    })
    //click nút chi tiết payment
    $('#table-customer').on('click', '.btn-payment', function () {

        onBtnPaymentDetailClick(this);
    })
    $('#btn-filter').on('click', function () {
        console.log('NHÁN FILTER')
        onBtnFIlterClick();
        // onBtnLogOutClick()
    })

})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnFIlterClick() {
    console.log('ĐANG FILTER')
    callApitoFilter().then(res => {
        console.log('CHECK RES FILTER', res)
        glistCustomer = res;
        customizeData();
    }).catch(e => {
        console.log(e)
    })
}
//hàm filter
function callApitoFilter() {
    let obj = {
        rank: 0,
        sumOrder: 0
    }
    obj.rank = $("#select-rank").val()
    obj.sumOrder = $("#select-sumorder").val()
    return new Promise((resolve, reject) => {
        $.ajax({
            async: false,
            url: `http://localhost:8080/filter_customer`,
            type: 'POST',
            async: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(obj),
            success: function (res) {

                resolve(res)
            },
            error: function (error) {
                reject(error)
            }
        })
    })

}
//custom data
function customizeData() {
    console.log('ĐANG CUSTOM BẢNG')
    glistCustomer.map(item => {
        item.sumOrder = item.orders.length
        item.fullname = item.lastName + " " + item.firstName
        item.total = 0;
        for (let bI = 0; bI < item.payments.length; bI++) {
            console.log('ĐANG XỬ LÍ PAYMENT')
            item.total += item.payments[bI].amount
        }
        return item
    })
    console.log("check customer", glistCustomer)
    displayTable(glistCustomer)
}
//hàm hiển thị bảng
function displayTable(paramData) {
    table.clear();
    table.rows.add(paramData);
    table.draw();

}
//hàm thực hiện khi nhấn nút export to excel
function onBtnExportToExcelClick() {
    window.open(' http://localhost:8080/export/customers/excel ');
}
//hàm gọi api load danh sách album từ server khi tải trang
function onPageLoading() {
    console.log('làm toi day')
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/customer/all",
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)

                // console.log('check data get all customr from server', res);

            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm gọi api load danh sách order
function callAPIToGetOrder() {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/order/all",
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm gọi api load danh sách order
function callAPIToGetPayment() {

    return new Promise((resolve, reject) => {
        $.ajax({
            url: "http://localhost:8080/payment/all",
            type: "GET",
            dataType: "json",
            success: function (res) {
                resolve(res)
            },
            error: function (err) {
                reject(err)
            }
        })

    })

}
//hàm thực hiện khi nhấn nút add new album
function onBtnAddCustomerClick() {

    $("#create-customer-modal").modal("show");

}
//hàm thực hiện khi nhấn create album trên modal
function onBtnCreateCustomerOnModalClick() {
    createNewCustomer();
    $("#create-customer-modal").modal("hide");
}
//hàm thực hiện khi lúc nút edit
function onBtnEditCustomerClick(paramButton) {
    $("#update-customer-modal").modal("show");
    gCustomerIdSelected = getCustomerIdFromButton(paramButton)

    console.log('check id customer selected', gCustomerIdSelected)
    callApiToGetCustomerById(gCustomerIdSelected)
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateCustomer(gCustomerIdSelected);
    $("#update-customer-modal").modal("hide");

}
//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteCustomerClick(paramButton) {
    $("#delete-modal").modal("show");
    gCustomerIdSelected = getCustomerIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedCustomer();
    $("#delete-modal").modal("hide");

}
//hàm thực hiện khi nhấn nút order detail
function onBtnOrderDetailClick(paramButton) {

    gCustomerIdSelected = getCustomerIdFromButton(paramButton)
    console.log('check id customer selected', gCustomerIdSelected);
    window.location.href = `orderOfOneCustomer.html?id=${gCustomerIdSelected}`

    // callApiToGetCustomerById(gCustomerIdSelected)
}
//hàm thực hiện khi nhấn nút payment detail
function onBtnPaymentDetailClick(paramButton) {

    gCustomerIdSelected = getCustomerIdFromButton(paramButton)
    console.log('check id customer selected', gCustomerIdSelected);
    window.location.href = `paymentOfOneCustomer.html?id=${gCustomerIdSelected}`

    // callApiToGetCustomerById(gCustomerIdSelected)
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm trả ra id album khi nhấn nút edit
function getCustomerIdFromButton(paramButton) {
    // var vTableRow = $(paramButton).parents("tr");
    var vTableRow = $(paramButton).parents("tr");
    gRowCustomerSelected = table.row(vTableRow).data();
    console.log('CHECK TABLE ROW', gRowCustomerSelected)
    console.log(gRowCustomerSelected.id);
    return gRowCustomerSelected.id;

    // var vTableRow = $(paramButton).closest("tr")
    // console.log('CHECK TABLE ROW', vTableRow)

    // gRowCustomerSelected = $("#table-customer").DataTable().row(0).data();
    // console.log('check hàng đã chọn', gRowCustomerSelected);
    // return gRowCustomerSelected.id;
}
//hàm gọi api để lấy album cần edit theo id
function callApiToGetCustomerById(id) {

    $.ajax({
        url: `http://localhost:8080/customer/details/${id}`,
        async: false,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check data customer need to update ress', res);
            if (glistCustomer && glistCustomer.length > 0) {
                glistCustomer.map((item, index) => {
                    if (item.id == res.id) {
                        glistCustomer.splice(index, 1)
                    }
                })
            }
            showDataUpdateOnForm(res);
        },
        error: function (err) {
            console.log(err.response);
        }
    })

}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(paramAlbum) {


    $('#input-address-update').val(paramAlbum.address);
    $('#input-city-update').val(paramAlbum.city);
    $('#input-country-update').val(paramAlbum.country);
    $('#input-credit-limit-update').val(paramAlbum.creditLimit);
    $('#input-firstname-update').val(paramAlbum.firstName);
    $('#input-lastname-update').val(paramAlbum.lastName);
    $('#input-phonenumber-update').val(paramAlbum.phoneNumber);
    $('#input-postalcode-update').val(paramAlbum.phoneNumber);
    $('#input-salerepemployeenumber-update').val(paramAlbum.salesRepEmployeeNumber);
    $('#input-state-update').val(paramAlbum.state);

}

function updateCustomer(paramAlbumId) {
    let objData = {
        address: '',
        city: '',
        country: '',
        creditLimit: 0,
        firstName: '',
        lastName: '',
        phoneNumber: '',
        postalCode: '',
        salesRepEmployeeNumber: 0,
        state: '',
    }
    let objDataFromForm = getDataOnFormUpdate(objData);
    callApiToUpdateData(paramAlbumId, objDataFromForm);
    onPageLoading();
    // var vCheck = validateDataOnform(objDataFromForm);
    // if (vCheck) {
    // 	let albumExist = '';
    // 	if (glistCustomer && glistCustomer.length > 0) {

    // 		for (let i = 0; i < glistCustomer.length; i++) {
    // 			if (glistCustomer[i].albumCode == objDataFromForm.albumCode) {
    // 				alert("Album existed");
    // 				albumExist = glistCustomer[i];
    // 				console.log('Album existed', albumExist)
    // 			}
    // 		}

    // 	}
    // 	if (albumExist == '') {

    // 		callApiToUpdateData(paramAlbumId, objDataFromForm);
    // 		onPageLoading();
    // 	}



    // }
}




//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {


    obj.address = $('#input-address-update').val();
    obj.city = $('#input-city-update').val();
    obj.country = $('#input-country-update').val();
    obj.creditLimit = $('#input-credit-limit-update').val();
    obj.firstName = $('#input-firstname-update').val();
    obj.lastName = $('#input-lastname-update').val();
    obj.phoneNumber = $('#input-phonenumber-update').val();
    obj.postalCode = $('#input-postalcode-update').val();
    obj.salesRepEmployeeNumber = $('#input-salerepemployeenumber-update').val();
    obj.state = $('#input-state-update').val();
    return obj
}
//hàm gọi api để update album
function callApiToUpdateData(id, paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/customer/update/${id}`,
        type: 'PUT',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('check data update response', res);

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}

//save new album
function createNewCustomer() {
    let objData = {
        address: '',
        city: '',
        country: '',
        creditLimit: 0,
        firstName: '',
        lastName: '',
        phoneNumber: '',
        postalCode: '',
        salesRepEmployeeNumber: 0,
        state: '',

    }
    let objDataFromForm = getDataOnForm(objData);
    callApiToPostData(objDataFromForm);
    onPageLoading();
    // var vCheck = validateDataOnform(objDataFromForm);
    // if (vCheck) {
    // 	if (glistCustomer && glistCustomer.length > 0) {
    // 		glistCustomer.map(item => {
    // 			if (item.albumCode == objDataFromForm.albumCode) {
    // 				alert("Album existed")
    // 			} else {
    // 				console.log('check data post', objDataFromForm)
    // 				callApiToPostData(objDataFromForm);
    // 				onPageLoading();

    // 			}
    // 		})
    // 	}

    // }

}
//hàm thực hiện validate dữ liệu trên form
// function validateDataOnform(paramObjData) {
// 	if (!paramObjData.albumCode) {
// 		alert('Missing album code')
// 		return false;
// 	}
// 	if (!paramObjData.albumName) {
// 		alert('Missing album name')
// 		return false;
// 	}
// 	if (!paramObjData.description) {
// 		alert('Missing description')
// 		return false;
// 	}
// 	return true;


// }
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.address = $('#input-address').val();
    obj.city = $('#input-city').val();
    obj.country = $('#input-country').val();
    obj.creditLimit = $('#input-credit-limit').val();
    obj.firstName = $('#input-firstname').val();
    obj.lastName = $('#input-lastname').val();
    obj.phoneNumber = $('#input-phonenumber').val();
    obj.postalCode = $('#input-postalcode').val();
    obj.salesRepEmployeeNumber = $('#input-salerepemployeenumber').val();
    obj.state = $('#input-state').val();
    return obj
}
//hàm gọi api để tạo album mới
function callApiToPostData(paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/customer/create`,
        type: 'POST',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {

            console.log('check data post response', res);

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}
function deletedCustomer() {
    callApiToDeleteData(gCustomerIdSelected);
    onPageLoading();
}
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/customer/delete/${id}`,
        type: 'DELETE',
        async: false,
        // contentType: "application/json;charset=UTF-8",
        // data: JSON.stringify(paramOrderObject),
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}




