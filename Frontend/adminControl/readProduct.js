
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gPRODUCT_COLS = ["id", "productCode", "productName", "productDescription", "productScale", "productVendor", "quantityInStock", "buyPrice", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gPRODUCT_STT_COL = 0;
const gPRODUCT_CODE_COL = 1;

const gPRODUCT_NAME_COL = 2;
const gPRODUCT_DES_COL = 3;
const gPRODUCT_SCALE_COL = 4;
const gPRODUCT_VENDOR_COL = 5;
const gPRODUCT_QUANTITY_COL = 6;
const gPRODUCT_PRICE_COL = 7;
const gPRODUCT_ACTION_COL = 8;


var table = $("#order-table").DataTable({
    columns: [
        { data: gPRODUCT_COLS[gPRODUCT_STT_COL] },
        { data: gPRODUCT_COLS[gPRODUCT_CODE_COL] },
        { data: gPRODUCT_COLS[gPRODUCT_NAME_COL] },
        { data: gPRODUCT_COLS[gPRODUCT_DES_COL] },
        { data: gPRODUCT_COLS[gPRODUCT_SCALE_COL] },
        { data: gPRODUCT_COLS[gPRODUCT_VENDOR_COL] },
        { data: gPRODUCT_COLS[gPRODUCT_QUANTITY_COL] },
        { data: gPRODUCT_COLS[gPRODUCT_PRICE_COL] },
        { data: gPRODUCT_COLS[gPRODUCT_ACTION_COL] },


    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gPRODUCT_ACTION_COL,
            defaultContent: `
					<i class="fas fa-edit edit-product"></i>
					<i class="fas fa-trash-alt delete-product"></i>
			  		`
        }
    ]
})
$(document).ready(function () {
    callApiToGetProductLine();
    onPageLoading()
    //create new photo
    $('#create-order').on('click', function () {
        onBtnAddProductClick();
    })
    $('#btn-create-product').on('click', function () {

        onBtnCreateOrderOnModalClick();
    })
    //edit photo
    $('#order-table').on('click', '.edit-product', function () {


        onBtnEditProductClick(this);
    })
    $('#btn-update-product').on('click', function () {

        onBtnUpdateOnModalClick()
    })

    //xóa photo
    $('#order-table').on('click', '.delete-product', function () {

        onBtnDeleteProductClick(this);
    })
    $('#btn-confirm-delete-product').on('click', function () {

        onBtnConfirmOnDeleteModalClick(this);
    })

})

var gRowSelected = ''
var gProductList = '';
var gProductLineIdSelected = '';
var glistOrder = [];
var gRowPhotoSelected = '';
var gProductIdSelected = ''

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */


//hàm thực hiện khi nhấn nút add new photo
function onBtnAddProductClick() {
    $("#create-order-modal").modal("show");

}

//hàm thực hiện khi lúc nút edit
function onBtnEditProductClick(paramButton) {
    $("#update-order-modal").modal("show");
    gProductIdSelected = getOrderIdFromButton(paramButton)

    console.log('check id product selected', gProductIdSelected)
    callApiToGetProductById(gProductIdSelected)
}
//hàm thực hiện khi nhấn nút update trên modal
function onBtnUpdateOnModalClick() {
    updateProduct(gProductIdSelected);
    $("#update-order-modal").modal("hide");


}

//Hàm thực hiện khi nhấn nút delete
function onBtnDeleteProductClick(paramButton) {
    $("#delete-modal").modal("show");
    gProductIdSelected = getOrderIdFromButton(paramButton)

}
//hàm thực hiện khi nhấn nút confirm trên delete modal
function onBtnConfirmOnDeleteModalClick() {
    deletedOrder();
    $("#delete-modal").modal("hide");

}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
/*** DELETE-------------------------------------------------------------------------- */
function deletedOrder() {
    callApiToDeleteData(gProductIdSelected);
    onPageLoading();
}
function callApiToDeleteData(id) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/product/delete/${id}`,
        type: 'DELETE',
        async: false,
        // contentType: "application/json;charset=UTF-8",
        // data: JSON.stringify(paramOrderObject),
        success: function (res) {
            alert('Delete success')

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}
/*** UPDATE-------------------------------------------------------------------------- */


function updateProduct(paramOrderId) {
    let objData = {
        productCode: '',
        productName: '',
        productDescription: '',
        productScale: '',
        productVendor: '',
        quantityInStock: 0,
        buyPrice: 0,
        salePrice: 0,
        brand: -1,
        color: -1,
        rate: -1

    }

    let objDataFromForm = getDataOnFormUpdate(objData);
    console.log("check UPDATE", objDataFromForm)
    callApiToUpdateData(paramOrderId, objDataFromForm);
    onPageLoading();

}

//hàm thu thập dư liệu trên form update
function getDataOnFormUpdate(obj) {
    obj.productCode = $('#input-product-code-update').val();
    obj.productName = $('#input-product-name-update').val();
    obj.productDescription = $('#input-product-description-update').val();
    obj.productScale = $('#input-product-scale-update').val();
    obj.productVendor = $('#input-product-vendor-update').val();
    obj.quantityInStock = $('#input-quantityInStock-update').val();
    obj.buyPrice = $('#input-buyPrice-update').val();
    obj.salePrice = $('#input-salePrice-update').val();

    obj.brand = $('#select-brand-update').val();
    obj.color = $('#select-color-update').val();
    obj.rate = $('#select-rate-update').val();
    return obj
}
//hàm gọi api để update album
function callApiToUpdateData(id, paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/product/update/${id}`,
        type: 'PUT',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('check data update photo response', res);

        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}


//hàm trả ra id photo khi nhấn nút edit
function getOrderIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    gRowPhotoSelected = table.row(vTableRow).data();
    console.log(gRowPhotoSelected.id);
    return gRowPhotoSelected.id;
}
//hàm gọi api để lấy photo cần edit theo id
function callApiToGetProductById(id) {

    $.ajax({
        url: `http://localhost:8080/product/details/${id}`,
        async: false,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check data product need to update ress', res);
            // if (glistAlbum && glistAlbum.length > 0) {
            // 	glistAlbum.map((item, index) => {
            // 		if (item.id == res.id) {
            // 			glistAlbum.splice(index, 1)
            // 		}
            // 	})
            // }
            showDataUpdateOnForm(res);
        },
        error: function (err) {
            console.log(err.response);
        }
    })

}
//hàm hiển thị data lên form edit
function showDataUpdateOnForm(paramProduct) {

    $('#input-product-code-update').val(paramProduct.productCode);
    $('#input-product-name-update').val(paramProduct.productName);
    $('#input-product-description-update').val(paramProduct.productDescription);
    $('#input-product-scale-update').val(paramProduct.productScale);
    $('#input-product-vendor-update').val(paramProduct.productVendor);
    $('#input-quantityInStock-update').val(paramProduct.quantityInStock);
    $('#input-buyPrice-update').val(paramProduct.buyPrice);
    $('#input-salePrice-update').val(paramProduct.salePrice);
    $('#select-brand-update').val(paramProduct.brand);
    $('#select-color-update').val(paramProduct.color);
    $('#select-rate-update').val(paramProduct.rate);

}

/*** CREATE-------------------------------------------------------------------------- */
//hàm thực hiện khi nhấn create photo trên modal
function onBtnCreateOrderOnModalClick() {
    createNewProduct();
    $("#create-order-modal").modal("hide");
}
//save new photo
function createNewProduct() {
    let objData = {
        productCode: '',
        productName: '',
        productDescription: '',
        productScale: '',
        productVendor: '',
        quantityInStock: 0,
        buyPrice: 0,
        salePrice: 0,
        photo: '',
        brand: -1,
        color: -1,
        rate: -1

    }
    let objDataFromForm = getDataOnForm(objData);
    console.log("check product object", objDataFromForm)
    callApiToPostData(objDataFromForm);
    onPageLoading();

    // var vCheck = validateDataOnform(objDataFromForm);
    // if (vCheck) {
    // 	if (glistOrder && glistOrder.length > 0) {
    // 		glistOrder.map(item => {
    // 			if (item.photoCode == objDataFromForm.photoCode) {
    // 				alert("photo existed")
    // 			} else {
    // 				console.log('check data post', objDataFromForm)
    // 				callApiToPostData(objDataFromForm);
    // 				onPageLoading();

    // 			}
    // 		})
    // 	}

    // }

}
//hàm thực hiện validate dữ liệu trên form
// function validateDataOnform(paramObjData) {
// 	if (!paramObjData.photoCode) {
// 		alert('Missing photo code')
// 		return false;
// 	}
// 	if (!paramObjData.photoName) {
// 		alert('Missing photo name')
// 		return false;
// 	}
// 	if (!paramObjData.photoLink) {
// 		alert('Missing photo link')
// 		return false;
// 	}
// 	if (!paramObjData.photoDescription) {
// 		alert('Missing photo description')
// 		return false;
// 	}
// 	return true;


// }
//hàm thu thập dư liệu trên form
function getDataOnForm(obj) {
    obj.productCode = $('#input-product-code').val();
    obj.productName = $('#input-product-name').val();
    obj.productDescription = $('#input-product-description').val();
    obj.productScale = $('#input-product-scale').val();
    obj.productVendor = $('#input-product-vendor').val();
    obj.quantityInStock = $('#input-quantityInStock').val();
    obj.buyPrice = $('#input-buyPrice').val();
    obj.salePrice = $('#input-salePrice').val();
    obj.photo = "images" + "/" + $('#input-photo')[0].files[0].name;
    obj.brand = $('#select-brand').val();
    obj.color = $('#select-color').val();
    obj.rate = $('#select-rate').val();
    return obj
}
//hàm gọi api để tạo album mới
function callApiToPostData(paramOrderObject) {
    $.ajax({
        async: false,
        url: `http://localhost:8080/product/create/${gProductLineIdSelected}`,
        type: 'POST',
        async: false,
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrderObject),
        success: function (res) {
            console.log('check data post response', res);
        },
        error: function (error) {
            console.assert(error.responseText)
        }
    })
}






function callApiToGetProductLine() {
    $.ajax({
        async: false,
        url: "http://localhost:8080/product_line/all",
        method: "GET",
        success: function (pObjRes) {
            gProductList = pObjRes
            loadDataToProductLineSelect(gProductList);
            console.log('check response customer', pObjRes)

        },
        error: function (pXhrObj) {
            console.log(pXhrObj);
        }
    });
}
var gProductSelectElement = $("#product-line-select");
//hàm load thông tin album vào ô select
function loadDataToProductLineSelect(pProductList) {

    for (i = 0; i < pProductList.length; i++) {
        var bProductOption = $("<option/>");
        bProductOption.prop("value", pProductList[i].id);
        bProductOption.prop("text", pProductList[i].productLine);
        gProductSelectElement.append(bProductOption);
    };
}
//hàm xử lí chọn album
gProductSelectElement.on("change", function () {
    if (gProductList && gProductList.length > 0) {
        gProductList.map(item => {
            if (item.id == $(this).val()) {
                gProductLineIdSelected = item.id
            }
        })
    }
    console.log('check productline id id', gProductLineIdSelected)
    onSelectProductLineChange();

})
function onSelectProductLineChange() {
    $.ajax({
        url: `http://localhost:8080/productline/${gProductLineIdSelected}/products`,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check product by id', res);
            glistOrder = res
            table.clear();
            table.rows.add(res);
            table.draw();
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}
function onPageLoading() {
    $.ajax({
        url: `http://localhost:8080/product/all`,
        type: "GET",
        dataType: "json",
        success: function (res) {
            console.log('check product by id', res);
            glistOrder = res
            table.clear();
            table.rows.add(res);
            table.draw();
        },
        error: function (err) {
            console.log(err.response);
        }
    })
}









