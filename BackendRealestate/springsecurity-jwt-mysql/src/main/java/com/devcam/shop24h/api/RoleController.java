package com.devcam.shop24h.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Role;
import com.devcam.shop24h.repository.RoleRepository;
import com.devcam.shop24h.service.RoleService;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class RoleController {
    @Autowired
    RoleService roleService;

    @GetMapping("/role")
    public ResponseEntity<List<Role>> getRole() {
        try {
            return roleService.getRole();

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
