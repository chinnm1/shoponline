package com.devcam.shop24h.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Office;

@Repository
public interface OfficeRepository extends JpaRepository<Office, Long> {
    @Query(value = "SELECT * FROM offices WHERE id=:id", nativeQuery = true)
    Optional<Office> findOfficeById(@Param("id") int id);

}
