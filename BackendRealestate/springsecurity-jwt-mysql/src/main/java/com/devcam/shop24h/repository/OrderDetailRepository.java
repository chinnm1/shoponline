package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.Orderdetail;

@Repository
public interface OrderDetailRepository extends JpaRepository<Orderdetail, Long> {
    @Query(value = "SELECT * FROM order_details WHERE id =:id", nativeQuery = true)
    Optional<Orderdetail> findOrderDetailById(@Param("id") int id);

    @Query(value = " SELECT order_details.* FROM order_details INNER JOIN orders ON order_details.order_id= orders.id INNER JOIN products ON order_details.product_id = products.id WHERE orders.id =:orderId AND products.id =:productId", nativeQuery = true)
    List<Orderdetail> findOrderDetailByOrderIdAndProductId(@Param("orderId") int orderId,
            @Param("productId") int productId);

    @Query(value = " SELECT order_details.* FROM order_details INNER JOIN orders ON order_details.order_id= orders.id  WHERE orders.id =:orderId ", nativeQuery = true)
    List<Orderdetail> findOrderDetailByOrderId(@Param("orderId") int orderId);

}
