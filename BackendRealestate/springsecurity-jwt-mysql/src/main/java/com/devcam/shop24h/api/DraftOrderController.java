package com.devcam.shop24h.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.DraftOrder;
import com.devcam.shop24h.repository.DraftOrderReposity;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class DraftOrderController {
    @Autowired
    DraftOrderReposity draftOrderReposity;

    @PostMapping("/draftorder/create")
    public ResponseEntity<Object> createdraft(@RequestBody DraftOrder draft) {

        try {
            Optional<DraftOrder> draftData = draftOrderReposity.findDraftByUserIdAndProductId(draft.getUserID(),
                    draft.getProductID());
            if (draftData.isPresent()) {
                DraftOrder newRole = draftData.get();
                newRole.setAmount(draft.getAmount());
                DraftOrder savedRole = draftOrderReposity.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            } else {
                DraftOrder newRole = new DraftOrder();
                newRole.setUserID(draft.getUserID());
                newRole.setProductID(draft.getProductID());
                newRole.setAmount(draft.getAmount());

                DraftOrder savedRole = draftOrderReposity.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified draft: " + e.getMessage());
        }
    }

    @PutMapping("/draftorder/update/{id}")
    public ResponseEntity<Object> updatedraft(@PathVariable("id") int id, @RequestBody DraftOrder draft) {
        try {
            Optional<DraftOrder> draftData = draftOrderReposity.findDraftById(id);
            if (draftData.isPresent()) {
                DraftOrder newdraft = draftData.get();
                newdraft.setUserID(draft.getUserID());
                newdraft.setProductID(draft.getProductID());
                DraftOrder saveddraft = draftOrderReposity.save(newdraft);
                return new ResponseEntity<>(saveddraft, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified draft: " + id + "  for update.");
        }

    }

    @GetMapping("/draftorder/details/{id}")
    public ResponseEntity<DraftOrder> getDraftById(@PathVariable int id) {
        try {
            Optional<DraftOrder> draftData = draftOrderReposity.findDraftById(id);
            if (draftData.isPresent()) {
                return new ResponseEntity<>(draftData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/draftorder/{userId}/details")
    public ResponseEntity<List<DraftOrder>> getDraftByUserId(@PathVariable("userId") int userid) {
        try {
            List<DraftOrder> draftData = draftOrderReposity.findDraftByUserId(userid);
            if (!draftData.isEmpty()) {
                return new ResponseEntity<>(draftData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/draftorder/delete/{userid}/{productid}")
    public ResponseEntity<Object> deleteDraftById(@PathVariable("userid") int userid,
            @PathVariable("productid") int productid) {
        try {
            Optional<DraftOrder> draftData = draftOrderReposity.findDraftByUserIdAndProductId(userid, productid);
            if (draftData.isPresent()) {
                DraftOrder DraftDelete = draftData.get();
                draftOrderReposity.delete(DraftDelete);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
