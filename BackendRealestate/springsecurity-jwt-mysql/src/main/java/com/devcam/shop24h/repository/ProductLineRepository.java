package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.ProductLine;

@Repository
public interface ProductLineRepository extends JpaRepository<ProductLine, Long> {
    @Query(value = "SELECT * FROM product_lines WHERE id = :id", nativeQuery = true)
    Optional<ProductLine> findProductLineById(@Param("id") int id);

}