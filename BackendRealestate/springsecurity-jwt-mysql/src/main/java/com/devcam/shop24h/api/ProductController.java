package com.devcam.shop24h.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.jaxb.SpringDataJaxb.PageRequestDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Product;
import com.devcam.shop24h.entity.ProductLine;
import com.devcam.shop24h.repository.ProductLineRepository;
import com.devcam.shop24h.repository.ProductRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class ProductController {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductLineRepository productLineRepository;

    @GetMapping("/product/details/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable int id) {
        try {
            Optional<Product> productData = productRepository.findProductById(id);
            if (productData.isPresent()) {
                return new ResponseEntity<>(productData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/product/create/{id}")
    public ResponseEntity<Object> createProduct(@PathVariable("id") int id, @RequestBody Product product) {
        try {
            Optional<ProductLine> productLineData = productLineRepository.findProductLineById(id);
            if (productLineData.isPresent()) {
                Product newRole = new Product();
                newRole.setBuyPrice(product.getBuyPrice());
                newRole.setProductCode(product.getProductCode());
                newRole.setProductDescription(product.getProductDescription());
                newRole.setProductName(product.getProductName());
                newRole.setProductScale(product.getProductScale());
                newRole.setProductVendor(product.getProductVendor());
                newRole.setQuantityInStock(product.getQuantityInStock());
                if (newRole.getQuantityInStock() > 0) {
                    newRole.setInStock(1);
                }
                newRole.setPhoto(product.getPhoto());
                newRole.setSalePrice(product.getSalePrice());
                if (newRole.getSalePrice() > 0) {
                    newRole.setInSale(1);
                }
                newRole.setBrand(product.getBrand());
                newRole.setColor(product.getColor());
                newRole.setRate(product.getRate());

                ProductLine _productLine = productLineData.get();
                newRole.setProduct_line(_productLine);

                Product savedRole = productRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Product: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/product/filter")
    public ResponseEntity<Object> filterProduct(@RequestBody Product product) {
        try {
            if (product.getPriceFrom() == 0 || product.getPriceTo() == 0 && product.getProductStatus() == 1) {
                List<Product> listProduct = productRepository.filterProductWithoutRangePrice(
                        product.getProductLineIdFromClient(), product.getColor(), product.getBrand(), product.getRate(),
                        0, 2);
                System.out.println(listProduct.toString());
                return new ResponseEntity<>(listProduct, HttpStatus.OK);
            } else if (product.getPriceFrom() == 0 || product.getPriceTo() == 0 && product.getProductStatus() == 2) {
                List<Product> listProduct = productRepository.filterProductWithoutRangePrice(
                        product.getProductLineIdFromClient(), product.getColor(), product.getBrand(), product.getRate(),
                        2, 0);
                System.out.println(listProduct.toString());
                return new ResponseEntity<>(listProduct, HttpStatus.OK);
            } else if (product.getProductStatus() == 2) {
                List<Product> listProduct = productRepository.filterProduct(product.getProductLineIdFromClient(),
                        product.getColor(), product.getBrand(), product.getRate(), product.getPriceFrom(),
                        product.getPriceTo(), 2, 0);
                System.out.println(listProduct.toString());
                return new ResponseEntity<>(listProduct, HttpStatus.OK);
            } else if (product.getProductStatus() == 1) {
                List<Product> listProduct = productRepository.filterProduct(product.getProductLineIdFromClient(),
                        product.getColor(), product.getBrand(), product.getRate(), product.getPriceFrom(),
                        product.getPriceTo(), 0, 2);
                System.out.println(listProduct.toString());
                return new ResponseEntity<>(listProduct, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified ");
        }

    }

    @PutMapping("/product/update/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable("id") int id, @RequestBody Product product) {
        try {
            Optional<Product> productData = productRepository.findProductById(id);
            if (productData.isPresent()) {
                Product newRole = productData.get();
                newRole.setBuyPrice(product.getBuyPrice());
                newRole.setProductCode(product.getProductCode());
                newRole.setProductDescription(product.getProductDescription());
                newRole.setProductName(product.getProductName());
                newRole.setProductScale(product.getProductScale());
                newRole.setProductVendor(product.getProductVendor());
                newRole.setQuantityInStock(product.getQuantityInStock());
                if (newRole.getQuantityInStock() > 0) {
                    newRole.setInStock(1);
                }

                newRole.setSalePrice(product.getSalePrice());
                if (newRole.getSalePrice() > 0) {
                    newRole.setInSale(1);
                }
                newRole.setBrand(product.getBrand());
                newRole.setColor(product.getColor());
                newRole.setRate(product.getRate());
                Product savedRole = productRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified product: " + id + "  for update.");
        }

    }

    @PutMapping("/product/update_confirm/{id}")
    public ResponseEntity<Object> updateProductQuantity(@PathVariable("id") int id, @RequestBody Product product) {
        try {
            Optional<Product> productData = productRepository.findProductById(id);
            if (productData.isPresent()) {
                Product newRole = productData.get();
                if (newRole.getQuantityInStock() > 0) {
                    newRole.setQuantityInStock(newRole.getQuantityInStock() - product.getBuyNumber());
                }

                Product savedRole = productRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to get specified product: " + id + "  for update.");
        }

    }

    @GetMapping("/product/all")
    public ResponseEntity<List<Product>> getAllProduct() {
        try {
            List<Product> product = new ArrayList<Product>();
            productRepository.findAll().forEach(product::add);
            return new ResponseEntity<>(product, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product/all/{numberPage}")
    public ResponseEntity<List<Product>> getAllRealestateByProjectId(@PathVariable("numberPage") int numberPage) {
        int Length = 6;
        int start = numberPage - 1;
        try {
            List<Product> listProduct = productRepository.findProductPageable(PageRequest.of(start, Length));
            if (!listProduct.isEmpty()) {
                return new ResponseEntity<>(listProduct, HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/productline/{productLineId}/products")
    public ResponseEntity<List<Product>> getProductByProductLineId(
            @PathVariable(value = "productLineId") int productLineId) {
        try {
            List<Product> productData = productRepository.findProductByProductLineId(productLineId);
            if (!productData.isEmpty()) {
                return new ResponseEntity<>(productData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/product/delete/{id}")
    public ResponseEntity<Object> deleteProductById(@PathVariable int id) {
        try {
            Optional<Product> productData = productRepository.findProductById(id);
            Product productDelete = productData.get();
            productRepository.delete(productDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
