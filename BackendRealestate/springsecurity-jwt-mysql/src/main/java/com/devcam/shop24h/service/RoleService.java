package com.devcam.shop24h.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcam.shop24h.entity.Role;
import com.devcam.shop24h.repository.RoleRepository;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    // lấy ra tát cả role
    public ResponseEntity<List<Role>> getRole() {

        List<Role> listRole = roleRepository.findAll();
        if (!listRole.isEmpty()) {
            return new ResponseEntity<>(listRole, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }
}
