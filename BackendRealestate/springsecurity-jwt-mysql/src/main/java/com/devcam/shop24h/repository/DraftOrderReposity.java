package com.devcam.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcam.shop24h.entity.DraftOrder;

@Repository
public interface DraftOrderReposity extends JpaRepository<DraftOrder, Long> {
    @Query(value = "SELECT * FROM draftorder WHERE id = :id", nativeQuery = true)
    Optional<DraftOrder> findDraftById(@Param("id") int id);

    @Query(value = "SELECT * FROM draftorder WHERE userid = :userid", nativeQuery = true)
    List<DraftOrder> findDraftByUserId(@Param("userid") int id);

    @Query(value = "SELECT * FROM draftorder WHERE userid = :userid AND productid=:productid", nativeQuery = true)
    Optional<DraftOrder> findDraftByUserIdAndProductId(@Param("userid") int userid, @Param("productid") int productid);
}
