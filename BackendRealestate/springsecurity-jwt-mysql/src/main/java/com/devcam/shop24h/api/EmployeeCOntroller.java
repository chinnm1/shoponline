package com.devcam.shop24h.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Employee;
import com.devcam.shop24h.repository.EmployeeRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class EmployeeCOntroller {
    @Autowired
    EmployeeRepository emplyeeRepository;

    @PostMapping("/employee/create")
    public ResponseEntity<Object> createemployee(@RequestBody Employee employee) {
        try {
            Employee newRole = new Employee();
            newRole.setEmail(employee.getEmail());
            newRole.setExtension(employee.getExtension());
            newRole.setFirstname(employee.getFirstname());
            newRole.setLastname(employee.getLastname());
            newRole.setJobTitle(employee.getJobTitle());
            newRole.setOfficeCode(employee.getOfficeCode());
            newRole.setReportTo(employee.getReportTo());

            Employee savedRole = emplyeeRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified employee: " + e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/employee/update/{id}")
    public ResponseEntity<Object> updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {
        try {
            Optional<Employee> employeeData = emplyeeRepository.findEmployeeById(id);
            if (employeeData.isPresent()) {
                Employee newRole = employeeData.get();
                newRole.setEmail(employee.getEmail());
                newRole.setExtension(employee.getExtension());
                newRole.setFirstname(employee.getFirstname());
                newRole.setLastname(employee.getLastname());
                newRole.setJobTitle(employee.getJobTitle());
                newRole.setOfficeCode(employee.getOfficeCode());
                newRole.setReportTo(employee.getReportTo());

                Employee savedcustomer = emplyeeRepository.save(newRole);
                return new ResponseEntity<>(savedcustomer, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified employee: " + id + "  for update.");
        }

    }

    @GetMapping("/employee/details/{id}")
    public ResponseEntity<Employee> getEmplyeeById(@PathVariable int id) {
        try {
            Optional<Employee> EmployeeData = emplyeeRepository.findEmployeeById(id);
            if (EmployeeData.isPresent()) {
                return new ResponseEntity<>(EmployeeData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/employee/all")
    public ResponseEntity<List<Employee>> getAllEmployee() {
        try {
            List<Employee> listEmployee = new ArrayList<Employee>();
            emplyeeRepository.findAll().forEach(listEmployee::add);

            return new ResponseEntity<>(listEmployee, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/employee/delete/{id}")
    public ResponseEntity<Object> deleteEmployeeById(@PathVariable int id) {
        try {
            Optional<Employee> EmployeeData = emplyeeRepository.findEmployeeById(id);
            Employee EmployeeDelete = EmployeeData.get();
            emplyeeRepository.delete(EmployeeDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
