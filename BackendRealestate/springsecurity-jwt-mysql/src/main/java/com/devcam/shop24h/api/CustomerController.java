package com.devcam.shop24h.api;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcam.shop24h.entity.Customer;
import com.devcam.shop24h.repository.CustomerRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/customer_detail_by_fristname/{firstname}")
    public ResponseEntity<Object> getUserById(@PathVariable(name = "firstname", required = true) String firstname) {
        List<Customer> userFounded = customerRepository.findCustomersByFirstNameLike(firstname);

        if (!userFounded.isEmpty()) {
            return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/filter_customer")
    public ResponseEntity<Object> filterCustomer(@RequestBody Customer customer) {
        try {
            if (customer.getRank() == 4) {
                List<Customer> userFounded = customerRepository.findCustomersPlatinum(customer.getSumOrder());
                if (!userFounded.isEmpty()) {
                    return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
                } else {
                    return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
                }
            } else if (customer.getRank() == 3) {
                List<Customer> userFounded = customerRepository.findCustomersGolde(customer.getSumOrder());
                if (!userFounded.isEmpty()) {
                    return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
                } else {
                    return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
                }
            } else if (customer.getRank() == 2) {
                List<Customer> userFounded = customerRepository.findCustomersSilver(customer.getSumOrder());
                if (!userFounded.isEmpty()) {
                    return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
                } else {
                    return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
                }
            } else if (customer.getRank() == 1) {
                List<Customer> userFounded = customerRepository.findCustomersVip(customer.getSumOrder());
                if (!userFounded.isEmpty()) {
                    return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
                } else {
                    return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
                }
            } else if (customer.getRank() == 0) {
                List<Customer> userFounded = customerRepository.findCustomersByOrder(customer.getSumOrder());
                if (!userFounded.isEmpty()) {
                    return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
                } else {
                    return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
                }
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer_by_country_orderby_firstname/{country}/{numberPage}")
    // @GetMapping("/customer_by_country_orderby_firstname/{country}")
    public ResponseEntity<List<Customer>> getCustomerByCountryOrderByFirstname(
            @PathVariable("country") String country, @PathVariable("numberPage") int numberPage) {
        // @PathVariable("country") String country) {

        int length = 3;
        int start = numberPage - 1;
        try {

            List<Customer> listCustomer = customerRepository.findCustomersByCountryOrderByFirstName(country,
                    PageRequest.of(start, length));
            if (!listCustomer.isEmpty()) {
                return new ResponseEntity<>(listCustomer, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer_by_city_and_state/{cỉty}/{state}/{numberPage}")
    public ResponseEntity<List<Customer>> getCustomerByCityAndState(
            @PathVariable("city") String city, @PathVariable("state") String state,
            @PathVariable("numberPage") int numberPage) {
        // @PathVariable("country") String country) {

        int length = 3;
        int start = numberPage - 1;
        try {

            List<Customer> listCustomer = customerRepository.findCustomersByCityAndState(city, state,
                    PageRequest.of(start, length));
            if (!listCustomer.isEmpty()) {
                return new ResponseEntity<>(listCustomer, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update_contry/{country}/{id}")

    public ResponseEntity<Object> updateCountry(@PathVariable("country") String country,
            @PathVariable("id") int id) {
        try {
            System.out.println("--------------------------------------");
            System.out.println("----------------LAM DAN DAY-------------------------");
            int countryUpdate = customerRepository.updateCountry(country, id);

            return new ResponseEntity<>(countryUpdate, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getMessage());
        }
    }

    @PostMapping("/customer/create")
    public ResponseEntity<Object> createcustomer(@RequestBody Customer customer) {

        try {
            Optional<Customer> customerData = customerRepository.findCustomersByPhone(customer.getPhoneNumber());
            if (customerData.isPresent()) {
                Customer newRole = customerData.get();
                newRole.setAddress(customer.getAddress());
                newRole.setCity(customer.getCity());
                newRole.setCountry(customer.getCountry());
                newRole.setCreditLimit(customer.getCreditLimit());
                newRole.setFirstName(customer.getFirstName());
                newRole.setLastName(customer.getLastName());
                newRole.setPhoneNumber(customer.getPhoneNumber());
                newRole.setPostalCode(customer.getPostalCode());
                newRole.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
                newRole.setState("Open");
                Customer savedcustomer = customerRepository.save(newRole);
                return new ResponseEntity<>(savedcustomer, HttpStatus.OK);
            } else {
                Customer newRole = new Customer();
                newRole.setAddress(customer.getAddress());
                newRole.setCity(customer.getCity());
                newRole.setCountry(customer.getCountry());
                newRole.setCreditLimit(customer.getCreditLimit());
                newRole.setLastName(customer.getLastName());
                newRole.setFirstName(customer.getFirstName());
                newRole.setPhoneNumber(customer.getPhoneNumber());
                newRole.setPostalCode(customer.getPostalCode());
                newRole.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
                newRole.setState("Open");

                Customer savedRole = customerRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/customer/update/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") int id, @RequestBody Customer customer) {
        try {
            Optional<Customer> customerData = customerRepository.findCustomerById(id);
            if (customerData.isPresent()) {
                Customer newRole = customerData.get();
                newRole.setAddress(customer.getAddress());
                newRole.setCity(customer.getCity());
                newRole.setCountry(customer.getCountry());
                newRole.setCreditLimit(customer.getCreditLimit());
                newRole.setFirstName(customer.getFirstName());
                newRole.setLastName(customer.getLastName());
                newRole.setPhoneNumber(customer.getPhoneNumber());
                newRole.setPostalCode(customer.getPostalCode());
                newRole.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
                newRole.setState(customer.getState());

                Customer savedcustomer = customerRepository.save(newRole);
                return new ResponseEntity<>(savedcustomer, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {

            return ResponseEntity.badRequest().body("Failed to get specified customer: " + id + "  for update.");
        }

    }

    @GetMapping("/customer/details/{id}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable int id) {
        try {
            Optional<Customer> CustomerData = customerRepository.findCustomerById(id);
            if (CustomerData.isPresent()) {
                return new ResponseEntity<>(CustomerData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/customer/all")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        try {
            List<Customer> listCustomer = new ArrayList<Customer>();
            customerRepository.findAll().forEach(listCustomer::add);

            return new ResponseEntity<>(listCustomer, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/customer/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable int id) {
        try {
            Optional<Customer> CustomerData = customerRepository.findCustomerById(id);
            Customer CustomerDelete = CustomerData.get();
            customerRepository.delete(CustomerDelete);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // @GetMapping("/export/customers/excel")
    // public void exportToExcel(HttpServletResponse response) throws IOException {
    // response.setContentType("application/octet-stream");
    // DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    // String currentDateTime = dateFormatter.format(new Date());

    // String headerKey = "Content-Disposition";
    // String headerValue = "attachment; filename=users_" + currentDateTime +
    // ".xlsx";
    // response.setHeader(headerKey, headerValue);

    // List<Customer> customer = new ArrayList<Customer>();

    // customerRepository.findAll().forEach(customer::add);

    // ExcelExporter excelExporter = new ExcelExporter(customer);

    // excelExporter.export(response);
    // }

}
