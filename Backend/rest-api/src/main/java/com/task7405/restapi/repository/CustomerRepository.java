package com.task7405.restapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.*;

import com.task7405.restapi.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    // Viết query cho bảng customers cho phép tìm danh sách theo họ hoặc tên với
    // LIKE

    @Query(value = "SELECT * FROM customers WHERE first_name LIKE %:firstname%", nativeQuery = true)
    List<Customer> findCustomersByFirstNameLike(@Param("firstname") String firstname);

    @Query(value = "SELECT * FROM customers WHERE phone_number =:phone", nativeQuery = true)
    Optional<Customer> findCustomersByPhone(@Param("phone") String phone);

    // Viết query cho bảng customers cho phép tìm danh sách theo country có phân
    // trang và ORDER BY tên.
    @Query(value = "SELECT * FROM customers WHERE country = ?1 ORDER BY first_name", nativeQuery = true)
    List<Customer> findCustomersByCountryOrderByFirstName(String country, Pageable pageable);

    @Query(value = "SELECT customers.*, SUM(payments.amount) AS sum , COUNT(orders.id) as sumOrder FROM customers INNER JOIN payments ON customers.id= payments.customer_id INNER JOIN orders ON customers.id= orders.customer_id GROUP BY customers.id HAVING sum >50000000  AND sumOrder > :sumOrder", nativeQuery = true)
    List<Customer> findCustomersPlatinum(@Param("sumOrder") int sumOrder);

    @Query(value = "SELECT customers.*, SUM(payments.amount) AS sum, COUNT(orders.id) as sumOrder FROM customers INNER JOIN payments ON customers.id= payments.customer_id INNER JOIN orders ON customers.id= orders.customer_id GROUP BY customers.id HAVING sum >20000000 AND sum <= 50000000  AND sumOrder >:sumOrder", nativeQuery = true)
    List<Customer> findCustomersGolde(@Param("sumOrder") int sumOrder);

    @Query(value = "SELECT customers.*, SUM(payments.amount) AS sum, COUNT(orders.id) as sumOrder FROM customers INNER JOIN payments ON customers.id= payments.customer_id INNER JOIN orders ON customers.id= orders.customer_id GROUP BY customers.id HAVING sum >10000000 AND sum <= 20000000  AND sumOrder >:sumOrder", nativeQuery = true)
    List<Customer> findCustomersSilver(@Param("sumOrder") int sumOrder);

    @Query(value = "SELECT customers.*, SUM(payments.amount) AS sum, COUNT(orders.id) as sumOrder FROM customers INNER JOIN payments ON customers.id= payments.customer_id INNER JOIN orders ON customers.id= orders.customer_id GROUP BY customers.id HAVING sum >5000000 AND sum <= 10000000  AND sumOrder >:sumOrder", nativeQuery = true)
    List<Customer> findCustomersVip(@Param("sumOrder") int sumOrder);

    @Query(value = "SELECT customers.*, COUNT(orders.id) as sumOrder FROM customers INNER JOIN orders ON customers.id= orders.customer_id GROUP BY customers.id HAVING  sumOrder > :sumOrder", nativeQuery = true)
    List<Customer> findCustomersByOrder(@Param("sumOrder") int sumOrder);

    // Viết query cho bảng customers cho phép tìm danh sách theo city, state với
    // LIKE có phân trang.
    @Query(value = "SELECT * FROM customers WHERE city =?1 AND state =?2", nativeQuery = true)
    List<Customer> findCustomersByCityAndState(String city, String state, Pageable pageable);
    // Viết query cho bảng customers cho phép UPDATE dữ liệu có country = NULL với
    // giá trị truyền vào từ tham số

    @Transactional
    @Modifying
    @Query(value = "UPDATE customers SET country=?1 WHERE id=?2", nativeQuery = true)
    int updateCountry(String country, int id);

    @Query(value = "SELECT * FROM customers WHERE id=:id", nativeQuery = true)
    Optional<Customer> findCustomerById(@Param("id") int id);

}
