package com.task7405.restapi.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.task7405.restapi.model.Payment;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    @Query(value = "SELECT * FROM payments WHERE id =:id", nativeQuery = true)
    Optional<Payment> findPaymentById(@Param("id") int id);

    @Query(value = " SELECT payments.* FROM payments INNER JOIN customers ON payments.customer_id = customers.id WHERE customers.id = :customerId", nativeQuery = true)
    List<Payment> findPaymentByCustomerId(@Param("customerId") int customerId);

    @Query(value = "SELECT payments.payment_date, SUM(payments.amount) AS sum from payments GROUP BY payments.payment_date", nativeQuery = true)
    List<Object> findTotalMoneyByDate();

    @Query(value = "SELECT WEEK(payment_date) AS Week, SUM(amount) AS Total FROM payments GROUP BY Week", nativeQuery = true)
    List<Object> findTotalMoneyByWeek();

    @Query(value = "SELECT customer_id, SUM(amount) FROM payments GROUP BY customer_id HAVING SUM(amount) > 5000000", nativeQuery = true)
    List<Object> findAmountGroupBYCustomerId();

    // @Query(value = "SELECT firstname, lastname FROM SD_User WHERE id = ?1",
    // nativeQuery = true)
    // date findByNativeQuery(Integer id);

    public static interface dateOnly {

        Date getPaymentDate();

    }

}
