package com.task7405.restapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.task7405.restapi.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
        @Query(value = "SELECT * FROM products WHERE id =:id", nativeQuery = true)
        Optional<Product> findProductById(@Param("id") int id);

        @Query(value = "SELECT * FROM products", nativeQuery = true)
        List<Product> findProductPageable(Pageable pageable);

        @Query(value = " SELECT products.* FROM products INNER JOIN product_lines ON products.product_line_id = product_lines.id WHERE product_lines.id=:productLineId", nativeQuery = true)
        List<Product> findProductByProductLineId(@Param("productLineId") int productLineId);

        @Query(value = " SELECT * FROM `products` INNER JOIN product_lines ON products.product_line_id= product_lines.id WHERE (products.product_line_id LIKE :productLineId OR '%'=:productLineId ) AND(products.color LIKE :color OR '%'=:color)AND (products.brand LIKE :brand OR '%'=:brand)AND (products.rate LIKE :rate OR '%'=:rate)AND ( :priceFrom<products.buy_price<:priceTo)AND(products.in_sale LIKE :inSale OR '%'=:inSale)AND (products.in_stock LIKE :inStock OR '%'=:inStock)", nativeQuery = true)
        List<Product> filterProduct(
                        @Param("productLineId") int productLineId,
                        @Param("color") int color,
                        @Param("brand") int brand,
                        @Param("rate") int rate,
                        @Param("priceFrom") float priceFrom,
                        @Param("priceTo") float priceTo,
                        @Param("inStock") int inStock,
                        @Param("inSale") int inSale);

        @Query(value = " SELECT * FROM `products` INNER JOIN product_lines ON products.product_line_id= product_lines.id WHERE (products.product_line_id LIKE :productLineId OR '%'=:productLineId ) AND(products.color LIKE :color OR '%'=:color)AND (products.brand LIKE :brand OR '%'=:brand)AND (products.rate LIKE :rate OR '%'=:rate)AND(products.in_sale LIKE :inSale OR '%'=:inSale)AND (products.in_stock LIKE :inStock OR '%'=:inStock)", nativeQuery = true)
        List<Product> filterProductWithoutRangePrice(
                        @Param("productLineId") int productLineId,
                        @Param("color") int color,
                        @Param("brand") int brand,
                        @Param("rate") int rate,
                        @Param("inStock") int inStock,
                        @Param("inSale") int inSale);

}
